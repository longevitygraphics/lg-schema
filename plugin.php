<?php
/**
 * Plugin Name: LG Schema
 * Plugin URI: http://www.mywebsite.com/my-first-plugin
 * Description: Schema structured data
 * Version: 1.0
 * Author: Gurpreet Dhanju
 * Author URI: https://www.longevitygraphics.com/
 */

require "acf.php";
require "structured-data-test-button.php";

add_action('wp_head', 'add_structured_data');
/**
 * add structured data to the site
 */
function add_structured_data()
{
	//get the active schema types
	$activate_schema_types = get_field('activate_schema_types', 'option');

	if (is_array($activate_schema_types) and in_array("LocalBusiness", $activate_schema_types)) {
		add_LocalBusiness_data();
	}

	if (is_array($activate_schema_types) and in_array("ContactPage", $activate_schema_types)) {
		add_ContactPage_data();
	}

	if (is_array($activate_schema_types) and in_array("AboutPage", $activate_schema_types)) {
		add_AboutPage_data();
	}
}


/**
 * add LocalBusiness structured data
 */
function add_LocalBusiness_data()
{

	$company_logo = get_company_logo();

	$schema = array(
		'@context' => "http://schema.org",
		'@type' => 'LocalBusiness',
		'name' => get_bloginfo('name'),
		'url' => get_home_url(),
		'address' => array(
			'@type' => 'PostalAddress',
			'streetAddress' => get_street_address(),
			'postalCode' => get_postal_code(),
			'addressLocality' => get_city(),
			'addressRegion' => get_province(),
			'addressCountry' => get_country()
		),
		'logo' => $company_logo,
		'image' => $company_logo,
		'telephone' => get_phone()
	);

	echo '<!-- LocalBusiness -->';
	echo '<script type="application/ld+json">' . json_encode($schema) . '</script>';

}


/**
 * add ContactPage structured data
 */
function add_ContactPage_data()
{
	global $wp;
	$current_url = rtrim(home_url(add_query_arg(array(), $wp->request)), '/\\');
	$contact_page = rtrim(get_field('lg_schema_contact_page', 'option'), '/\\');
	$site_type = get_site_type_from_yoast();
	if ($current_url != $contact_page) {
		return;
	}

	$schema = array(
		'@context' => 'http://schema.org',
		'@type' => 'ContactPage',
		'mainEntityOfPage' =>
			array(
				'@type' => 'WebPage',
				'@id' => $contact_page,
			),
		'url' => $contact_page,
		'headline' => '...',
		'publisher' =>
			array(
				'@type' => ucfirst($site_type),
				'@id' => get_bloginfo('url') . '#' . $site_type,
				'name' => get_bloginfo('name'),
				'logo' => get_logo_data()
			),
	);

	echo '<!-- ContactPage -->';
	echo '<script type="application/ld+json">' . json_encode($schema) . '</script>';
}


/**
 * add AboutPage structured data
 */
function add_AboutPage_data()
{
	global $wp;
	$current_url = rtrim(home_url(add_query_arg(array(), $wp->request)), '/\\');
	$about_page = rtrim(get_field('lg_schema_about_page', 'option'), '/\\');
	$site_type = get_site_type_from_yoast();
	if ($current_url != $about_page) {
		return;
	}

	$schema = array(
		'@context' => 'http://schema.org',
		'@type' => 'AboutPage',
		'mainEntityOfPage' =>
			array(
				'@type' => 'WebPage',
				'@id' => $about_page,
			),
		'url' => $about_page,
		'headline' => '...',
		'publisher' =>
			array(
				'@type' => ucfirst($site_type),
				'@id' => get_bloginfo('url') . '#' . $site_type,
				'name' => get_bloginfo('name'),
				'logo' => get_logo_data()
			),
	);

	echo '<!-- AboutPage -->';
	echo '<script type="application/ld+json">' . json_encode($schema) . '</script>';
}


/**
 * add lg schema options page to admin sidebar under Settings
 */
if (function_exists('acf_add_options_page')) {
	acf_add_options_sub_page(array(
		'page_title' => 'LG Schema Settings',
		'menu_title' => 'LG Schema Settings',
		'parent_slug' => 'options-general.php'
	));
}


/**
 * This function returns company logo from Yoast setting "Choose whether the site represents an organization or a person."
 * under "Search Appearance"
 * @return mixed|null
 */
function get_company_logo()
{
	$company_logo = WPSEO_Options::get('company_logo');
	if (empty($company_logo)) {
		$custom_logo_id = get_theme_mod('custom_logo');
		$image = wp_get_attachment_image_src($custom_logo_id, 'full');
		if (!empty($image) and !empty($image[0])) {
			$company_logo = $image[0];
		}
	}
	return $company_logo;
}

/**
 * get the logo url, width and height
 * @return array
 */
function get_logo_data()
{
	$custom_logo_id = get_theme_mod('custom_logo');
	$logo = wp_get_attachment_image_src($custom_logo_id, 'full');
	$logo_data = array();
	if ($logo and is_array($logo)) {
		$logo_data = array(
			'@type' => 'ImageObject',
			'url' => isset($logo[0]) ? $logo[0] : '',
			'width' => isset($logo[1]) ? $logo[1] : '',
			'height' => isset($logo[2]) ? $logo[2] : ''
		);
	}
	return $logo_data;
}


/**
 * Get the type of site from Yoast setting "Choose whether the site represents an organization or a person."
 * under "Search Appearance"
 * @return mixed|string|null
 */
function get_site_type_from_yoast()
{
	$site_type = WPSEO_Options::get('company_or_person');
	if ('company' === strtolower($site_type)) {
		$site_type = 'organization';
	}

	return $site_type;
}

/**
 * old framework used [lg-address1] but new framework is using [lg-address]
 * This function checks if shortcode exists then get the value.
 * If shortcode does not exist it check the next one
 */
function get_street_address()
{
	if (shortcode_exists('lg-address')) {
		$address = do_shortcode('[lg-address]');
		if (!empty($address)) {
			return $address;
		}
	}
	if (shortcode_exists('lg-address1')) {
		$address = do_shortcode('[lg-address1]');
		if (!empty($address)) {
			return $address;
		}
	}
	return get_field('lg_schema_street_address', 'option');
}

/**
 * This function checks the postal code from shortcode otherwise
 * it fetch from 'lg_schema_postal_code' field
 * @return mixed|string
 */
function get_postal_code()
{
	if (shortcode_exists('lg-postcode')) {
		$postal_code = do_shortcode('[lg-postcode]');
		if (!empty($postal_code)) {
			return $postal_code;
		}
	}
	return get_field('lg_schema_postal_code', 'option');
}


/**
 * This function checks the city from shortcode otherwise
 * it fetch from 'lg_schema_city' field
 * @return mixed|string
 */
function get_city()
{
	if (shortcode_exists('lg-city')) {
		$city = do_shortcode('[lg-city]');
		if (!empty($city)) {
			return $city;
		}
	}
	return get_field('lg_schema_city', 'option');
}


/**
 * This function checks the province from shortcode otherwise
 * it fetch from 'lg_schema_province_state' field
 * @return mixed|string
 */
function get_province()
{
	if (shortcode_exists('lg-province')) {
		$province = do_shortcode('[lg-province]');
		if (!empty($province)) {
			return $province;
		}
	}
	return get_field('lg_schema_province_state', 'option');
}


/**
 * This function checks the country from shortcode otherwise
 * it fetch from 'lg_schema_country' field
 * @return mixed|string
 */
function get_country()
{
	if (shortcode_exists('lg-country')) {
		$country = do_shortcode('[lg-country]');
		if (!empty($country)) {
			return $country;
		}
	}
	return get_field('lg_schema_country', 'option');
}

/**
 * This function checks the country from shortcode otherwise
 * it fetch from 'lg_schema_country' field
 * @return mixed|string
 */
function get_phone()
{
	if (shortcode_exists('lg-phone-main')) {
		$phone = do_shortcode('[lg-phone-main]');
		if (!empty($phone)) {
			return $phone;
		}
	}
	return get_field('lg_schema_phone', 'option');
}
